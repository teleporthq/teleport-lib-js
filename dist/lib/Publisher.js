"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Publisher = /** @class */ (function () {
    function Publisher(name) {
        this.type = 'publisher';
        this.name = name;
    }
    return Publisher;
}());
exports.default = Publisher;
//# sourceMappingURL=Publisher.js.map