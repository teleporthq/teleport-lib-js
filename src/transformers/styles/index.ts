import * as jsstocss from './jsstocss'

const styleTransformers = {
  jsstocss
}

export default styleTransformers
